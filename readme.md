# Account Scout

Search for certain user accounts on popular websites on the internet

## Installation

### With pip package manager

```bash
pip install accscout
```

### From repository

Download repository

```bash
git clone https://gitlab.com/richardnagy/security/accscout
cd accscout
```

Run setup script

> Make sure you use the correct python command. On Linux systems it's python3 by default.

```bash
python setup.py install
```

## Usage

After installation, simply use the command with the username you're searching for

```bash
accscout [USERNAME]
```

## Example

![demo run of the app](/assets/demo.png)

## License

Standard MIT license ([view](/LICENSE))
